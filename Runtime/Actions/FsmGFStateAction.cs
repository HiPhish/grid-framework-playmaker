using UnityEngine;

namespace HutongGames.PlayMaker.Actions {
	[ActionCategory("Grid Framework")]
	/// <summary>
	///   Abstract base class for all Playmaker actions involving Grid Framework.
	/// </summary>
	public abstract class FsmGFStateAction<T> : FsmStateAction where T: MonoBehaviour {
		/// <summary>
		///   Whether to run the action every frame.
		/// </summary>
		/// <remarks>
		///   If the action is running every frame it will never call the <c>Finish()</c> method.
		/// </remarks>
		public bool _everyFrame;

		public override void Reset() {
			_everyFrame = false;
		}

		public override void OnEnter() {
			DoAction();

			if (!_everyFrame) {
				Finish();
			}
		}

		public override void OnUpdate() {
			DoAction();
		}

		/// <summary> This is where the action itself is performed. </summary>
		protected abstract void DoAction();
	}
}
