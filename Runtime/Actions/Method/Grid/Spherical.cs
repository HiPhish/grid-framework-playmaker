﻿using GridFramework.Grids;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public class ConvertSphereGridCoordinates: FsmGFStateAction<SphereGrid> {
		enum CoordinateSystem { World, Grid, Spheric, Geographic }

		[Tooltip("Grid instance")]
		[ObjectType(typeof(SphereGrid))]
		[RequiredField]
		public FsmObject _grid;

		[Title("From coordinate system")]
		[ObjectType(typeof(CoordinateSystem))]
		public FsmEnum _fromSystem;

		public FsmVector3 _fromValue;

		[Title("To coordinate system")]
		[ObjectType(typeof(CoordinateSystem))]
		public FsmEnum _toSystem;

		[UIHint(UIHint.Variable)]
		public FsmVector3 _toValue;

		protected override void DoAction() {
			if (_fromValue.IsNone || _fromSystem.IsNone || _toSystem.IsNone) {
				throw new System.ArgumentNullException();
			}

			var grid = _grid.Value as SphereGrid;

			System.Func<Vector3, Vector3> converter = _fromSystem.Value switch {
				CoordinateSystem.World =>
					(_toSystem.Value switch {
						CoordinateSystem.Grid       => grid.WorldToGrid,
						CoordinateSystem.Geographic => grid.WorldToGeographic,
						CoordinateSystem.Spheric    => grid.WorldToSpheric,
						_                           => throw new System.NotImplementedException()
					}),
				CoordinateSystem.Grid =>
					(_toSystem.Value switch {
						CoordinateSystem.World      => grid.GridToWorld,
						CoordinateSystem.Geographic => grid.GridToGeographic,
						CoordinateSystem.Spheric    => grid.GridToSpheric,
						_                           => throw new System.NotImplementedException()
					}),
				CoordinateSystem.Geographic =>
					(_toSystem.Value switch {
						CoordinateSystem.World    => grid.GeographicToWorld,
						CoordinateSystem.Grid     => grid.GeographicToGrid,
						CoordinateSystem.Spheric  => grid.GeographicToSpheric,
						_                         => throw new System.NotImplementedException()
					}),
				CoordinateSystem.Spheric =>
					(_toSystem.Value switch {
						CoordinateSystem.World      => grid.SphericToWorld,
						CoordinateSystem.Grid       => grid.SphericToGrid,
						CoordinateSystem.Geographic => grid.SphericToGeographic,
						_                           => throw new System.NotImplementedException()
					}),
				_ => throw new System.NotImplementedException()
			};

			_toValue.Value = converter(_fromValue.Value);
		}
	}
}
