﻿using GridFramework.Grids;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public class ConvertPolarGridCoordinates: FsmGFStateAction<PolarGrid> {
		enum CoordinateSystem { World, Grid, Polar }

		[Tooltip("Grid instance")]
		[ObjectType(typeof(PolarGrid))]
		[RequiredField]
		public FsmObject _grid;

		[Title("From coordinate system")]
		[ObjectType(typeof(CoordinateSystem))]
		public FsmEnum _fromSystem;

		public FsmVector3 _fromValue;

		[Title("To coordinate system")]
		[ObjectType(typeof(CoordinateSystem))]
		public FsmEnum _toSystem;

		[UIHint(UIHint.Variable)]
		public FsmVector3 _toValue;

		protected override void DoAction() {
			if (_fromValue.IsNone || _fromSystem.IsNone || _toSystem.IsNone) {
				throw new System.ArgumentNullException();
			}

			var grid = _grid.Value as PolarGrid;

			System.Func<Vector3, Vector3> converter = _fromSystem.Value switch {
				CoordinateSystem.World =>
					(_toSystem.Value switch {
						CoordinateSystem.Grid  => grid.WorldToGrid,
						CoordinateSystem.Polar => grid.WorldToPolar,
						_                      => throw new System.NotImplementedException()
					}),
				CoordinateSystem.Grid =>
					(_toSystem.Value switch {
						CoordinateSystem.World => grid.GridToWorld,
						CoordinateSystem.Polar => grid.GridToPolar,
						_                      => throw new System.NotImplementedException()
					}),
				CoordinateSystem.Polar =>
					(_toSystem.Value switch {
						CoordinateSystem.World => grid.PolarToWorld,
						CoordinateSystem.Grid  => grid.PolarToGrid,
						_                      => throw new System.NotImplementedException()
					}),
				_ => throw new System.NotImplementedException()
			};

			_toValue.Value = converter(_fromValue.Value);
		}
	}
}
