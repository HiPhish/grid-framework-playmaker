﻿using GridFramework.Grids;
using UnityEngine;

// Missing: Any conversion to and from cubic

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public class ConvertHexGridCoordinates: FsmGFStateAction<HexGrid> {
		enum CoordinateSystem {
			World, HerringboneUp, HerringboneDown, RhombicUp, RhombicDown,
			// Cubic is missing, but I have no way of doing Vector4
		}

		[Tooltip("Grid instance")]
		[ObjectType(typeof(HexGrid))]
		[RequiredField]
		public FsmObject _grid;

		[Title("From coordinate system")]
		[ObjectType(typeof(CoordinateSystem))]
		public FsmEnum _fromSystem;

		public FsmVector3 _fromValue;

		[Title("To coordinate system")]
		[ObjectType(typeof(CoordinateSystem))]
		public FsmEnum _toSystem;

		[UIHint(UIHint.Variable)]
		public FsmVector3 _toValue;
		
		protected override void DoAction() {
			if (_fromValue.IsNone || _fromSystem.IsNone || _toSystem.IsNone) {
				throw new System.ArgumentNullException();
			}

			var grid = _grid.Value as HexGrid;

			System.Func<Vector3, Vector3> converter = _fromSystem.Value switch {
				CoordinateSystem.World =>
					(_toSystem.Value switch {
						CoordinateSystem.HerringboneUp   => grid.WorldToHerringUp,
						CoordinateSystem.HerringboneDown => grid.WorldToHerringDown,
						CoordinateSystem.RhombicUp       => grid.WorldToRhombicUp,
						CoordinateSystem.RhombicDown     => grid.WorldToRhombicDown,
						_                                => throw new System.NotImplementedException()
					}),
				CoordinateSystem.HerringboneUp =>
					(_toSystem.Value switch {
					 	CoordinateSystem.World           => grid.HerringUpToWorld,
					 	CoordinateSystem.HerringboneDown => grid.HerringUpToHerringDown,
					 	CoordinateSystem.RhombicUp       => grid.HerringUpToRhombicUp,
					 	CoordinateSystem.RhombicDown     => grid.HerringUpToRhombicDown,
						_                                => throw new System.NotImplementedException()
					}),
				CoordinateSystem.HerringboneDown =>
					(_toSystem.Value switch {
					 	CoordinateSystem.World           => grid.HerringDownToWorld,
					 	CoordinateSystem.HerringboneUp   => grid.HerringDownToHerringUp,
					 	CoordinateSystem.RhombicUp       => grid.HerringDownToRhombicUp,
					 	CoordinateSystem.RhombicDown     => grid.HerringDownToRhombicDown,
						_                                => throw new System.NotImplementedException()
					}),
				CoordinateSystem.RhombicUp =>
					(_toSystem.Value switch {
					 	CoordinateSystem.World           => grid.RhombicUpToWorld,
					 	CoordinateSystem.HerringboneUp   => grid.RhombicUpToHerringUp,
					 	CoordinateSystem.HerringboneDown => grid.RhombicUpToHerringDown,
					 	CoordinateSystem.RhombicDown     => grid.RhombicUpToRhombicDown,
						_                                => throw new System.NotImplementedException()
					}),
				CoordinateSystem.RhombicDown =>
					(_toSystem.Value switch {
					 	CoordinateSystem.World           => grid.RhombicDownToWorld,
					 	CoordinateSystem.HerringboneUp   => grid.RhombicDownToHerringUp,
					 	CoordinateSystem.HerringboneDown => grid.RhombicDownToHerringDown,
					 	CoordinateSystem.RhombicUp       => grid.RhombicDownToRhombicUp,
						_                                => throw new System.NotImplementedException()
					}),
				_ => throw new System.NotImplementedException()
			};

			_toValue.Value = converter(_fromValue.Value);
		}
	}
}
