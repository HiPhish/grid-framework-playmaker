﻿using UnityEngine;
using GridFramework.Grids;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessLayeredGrid<T>: FsmGFStateAction<T> where T: LayeredGrid {
		[Tooltip("Distance between two grid layers.")]
		public FsmFloat _dept;

		[Tooltip("Vector from one layer to the next one")]
		public FsmVector3 _forward;

		protected void GetValues(LayeredGrid grid) {
            _dept.Value = grid.Depth;
            _forward.Value = grid.Forward;
		}

		protected void SetValues(LayeredGrid grid) {
        	if (!_dept.IsNone) grid.Depth = _dept.Value;
        	if (!_forward.IsNone) {
        		var transform = grid.gameObject.transform;
        		transform.rotation = Quaternion.FromToRotation(Vector3.forward, _forward.Value);
        		grid.Depth = _forward.Value.magnitude;
        	}
		}
	}
}
