﻿using GridFramework.Grids;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessSphereGrid: FsmGFStateAction<SphereGrid> {
		[Tooltip("Grid instance")]
		[ObjectType(typeof(HexGrid))]
		[RequiredField]
		public FsmObject _grid;

		[Tooltip("Radius of the spherical grid.")]
		public FsmFloat _radius;

		[Tooltip("Radius of the spherical grid.")]
		public FsmInt _parallels;

		[Tooltip("Radius of the spherical grid.")]
		public FsmInt _meridians;

		[Tooltip("Angle between two parallels in the sphere grid (in radians).")]
		public FsmFloat _polar;

		[Tooltip("Angle between two parallels in the sphere grid (in degrees).")]
		public FsmFloat _polarDeg;

		[Tooltip("Angle between two meridians in the sphere grid (in radians).")]
		public FsmFloat _azimuth;

		[Tooltip("Angle between two meridians in the sphere grid (in degrees).")]
		public FsmFloat _azimuthDeg;
	}

	public class GetSphereGrid: AccessSphereGrid {
        protected override void DoAction() {
        	var grid = _grid.Value as SphereGrid;
			_radius.Value = grid.Radius;
			_parallels.Value = grid.Parallels;
			_meridians.Value = grid.Meridians;
			_polar.Value = grid.Polar;
			_polarDeg.Value = grid.PolarDeg;
			_azimuth.Value = grid.Azimuth;
			_azimuthDeg.Value = grid.AzimuthDeg;
        }
	}

	public class SetSphereGrid: AccessSphereGrid {
        protected override void DoAction() {
        	var grid = _grid.Value as SphereGrid;
			if (!_radius.IsNone) grid.Radius = _radius.Value;
			if (!_parallels.IsNone) grid.Parallels = _parallels.Value;
			if (!_meridians.IsNone) grid.Meridians = _meridians.Value;
			if (!_polar.IsNone) grid.Polar = _polar.Value;
			if (!_polarDeg.IsNone) grid.PolarDeg = _polarDeg.Value;
			if (!_azimuth.IsNone) grid.Azimuth = _azimuth.Value;
			if (!_azimuthDeg.IsNone) grid.AzimuthDeg = _azimuthDeg.Value;
        }
	}
}
