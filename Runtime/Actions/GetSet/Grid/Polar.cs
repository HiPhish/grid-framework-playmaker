﻿using GridFramework.Grids;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessPolarGrid: AccessLayeredGrid<PolarGrid> {
		[Tooltip("Grid instance")]
		[ObjectType(typeof(PolarGrid))]
		[RequiredField]
		public FsmObject _grid;

		[Tooltip("Radius of the polar grid.")]
		public FsmFloat _radius;

		[Tooltip("Amount of sectors in the polar grid.")]
		public FsmInt _sectors;

		[Tooltip("Angle between two sectors in the polar grid (in radians).")]
		public FsmFloat _radians;

		[Tooltip("Angle between two sectors in the polar grid (in degrees).")]
		public FsmFloat _degrees;
	}

	public class GetPolarGrid: AccessPolarGrid {
		[Tooltip("Rotation between two sectors in the polar grid.")]
		public FsmQuaternion _rotation;

		[Tooltip("Polar coordinates (1, 0, 0) in world-space.")]
		public FsmVector3 _right;

        protected override void DoAction() {
			var grid = _grid.Value as PolarGrid;
            this.GetValues(grid);
            _radius.Value = grid.Radius;
            _sectors.Value = grid.Sectors;
            _radians.Value = grid.Radians;
            _degrees.Value = grid.Degrees;
            _rotation.Value = grid.Rotation;
            _right.Value = grid.Right;
        }
	}

	public class SetPolarGrid: AccessPolarGrid {
        protected override void DoAction() {
        	var grid = _grid.Value as PolarGrid;
            this.SetValues(grid);
            if (!_radius.IsNone)  grid.Radius  =  _radius.Value;
            if (!_sectors.IsNone) grid.Sectors = _sectors.Value;
            if (!_radians.IsNone) grid.Radians = _radians.Value;
            if (!_degrees.IsNone) grid.Degrees = _degrees.Value;
        }
	}
}
