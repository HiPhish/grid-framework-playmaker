﻿using GridFramework.Grids;
// Missing: shearing (impossible)

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessRectGrid: FsmGFStateAction<RectGrid> {
		[Tooltip("Grid instance")]
		[ObjectType(typeof(HexGrid))]
		[RequiredField]
		public FsmObject _grid;

		[Tooltip("Spacing of the rectangular grid.")]
		public FsmVector3 _spacing;
	}

	public class GetRectGrid: AccessRectGrid {
		[Tooltip("The grid's local \"right\" direction scaled by the spacing.")]
		public FsmVector3 _right;

		[Tooltip("The grid's local \"up\" direction scaled by the spacing.")]
		public FsmVector3 _up;

		[Tooltip("The grid's local \"forward\" direction scaled by the spacing.")]
		public FsmVector3 _forward;

        protected override void DoAction() {
        	var grid = _grid.Value as RectGrid;
        	_spacing.Value = grid.Spacing;
        	_right.Value = grid.Right;
        	_up.Value = grid.Up;
        	_forward.Value = grid.Forward;
        }
	}

	public class SetRectGrid: AccessRectGrid {
        protected override void DoAction() {
        	var grid = _grid.Value as RectGrid;
        	if (!_spacing.IsNone) grid.Spacing = _spacing.Value;
        }
	}
}
