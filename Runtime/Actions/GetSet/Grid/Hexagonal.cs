﻿using GridFramework.Grids;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessHexGrid: AccessLayeredGrid<HexGrid> {
		[Tooltip("Grid instance")]
		[ObjectType(typeof(HexGrid))]
		public FsmObject _grid;

		[Tooltip("Radius of the hex grid's hexes, i.e. the distance form the centre to a vertex.")]
		public FsmFloat _radius;

		[Tooltip("The hex grid's \"side\", which is 1.5 times the radius.")]
		public FsmFloat _side;

		[Tooltip("The hex grid's \"height\", which is the full width of the hex.")]
		public FsmFloat _height;

		[Tooltip("The hex grid's \"width\", which is the distance between opposite vertices.")]
		public FsmFloat _width;

		[Tooltip("Pointy sides or flat sides.")]
		[ObjectType(typeof(HexGrid.Orientation))]
		public FsmEnum _sides;
	}

	public class GetHexGrid: AccessHexGrid {

        protected override void DoAction() {
			if (_grid.IsNone) {
				throw new System.ArgumentNullException();
			}
			var grid = _grid.Value as HexGrid;

            this.GetValues(grid);
            _radius.Value = grid.Radius;
            _side.Value = grid.Side;
            _height.Value = grid.Height;
            _width.Value = grid.Width;
            _sides.Value = grid.Sides;
        }
	}

	public class SetHexGrid: AccessHexGrid {
        protected override void DoAction() {
			if (_grid.IsNone) {
				throw new System.ArgumentNullException();
			}
			var grid = _grid.Value as HexGrid;

            this.SetValues(grid);
            if (!_radius.IsNone) { grid.Radius = _radius.Value; }
            if (!_side.IsNone)   { grid.Side   = _side.Value;   }
            if (!_height.IsNone) { grid.Height = _height.Value; }
            if (!_width.IsNone)  { grid.Width  = _width.Value;  }
            if (!_sides.IsNone)  { grid.Sides  = (HexGrid.Orientation) _sides.Value;  }
        }

	}
}
