﻿using GridFramework.Renderers.Hexagonal;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessConeRenderer: AccessGridRenderer<Cone> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Cone))]
		[RequiredField]
		public FsmObject _renderer;

		[Tooltip("Y coordinate of the origin of the renderer.")]
		public FsmInt _originX;

		[Tooltip("X coordinate of the origin of the renderer.")]
		public FsmInt _originY;
		 
		[Tooltip("Lower radius range of the renderer.")]
		public FsmInt _radiusFrom;

		[Tooltip("Upper radius range of the renderer.")]
		public FsmInt _radiusTo;

		[Tooltip("Lower hex range of the renderer.")]
		public FsmInt _hexFrom;

		[Tooltip("Upper hex range of the renderer.")]
		public FsmInt _hexTo;

		[Tooltip("Lower layer range of the renderer.")]
		public FsmFloat _layerFrom;

		[Tooltip("Upper layer range of the renderer.")]
		public FsmFloat _layerTo;
	}

	public class GetConeRenderer: AccessConeRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Cone;
			this.GetValues(renderer);
			_originX.Value = renderer.OriginX;
			_originY.Value = renderer.OriginY;
			_radiusFrom.Value = renderer.RadiusFrom;
			_radiusTo.Value = renderer.RadiusTo;
			_hexFrom.Value = renderer.HexFrom;
			_hexTo.Value = renderer.HexTo;
			_layerFrom.Value = renderer.LayerFrom;
			_layerTo.Value = renderer.LayerTo;
		}
	}

	public class SetConeRenderer: AccessConeRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Cone;
			this.SetValues(renderer);
			if (!_originX.IsNone)    renderer.OriginX    =    _originX.Value;
			if (!_originY.IsNone)    renderer.OriginY    =    _originY.Value;
			if (!_radiusFrom.IsNone) renderer.RadiusFrom = _radiusFrom.Value;
			if (!_radiusTo.IsNone)   renderer.RadiusTo   =   _radiusTo.Value;
			if (!_hexFrom.IsNone)    renderer.HexFrom    =    _hexFrom.Value;
			if (!_hexTo.IsNone)      renderer.HexTo      =      _hexTo.Value;
			if (!_layerFrom.IsNone)  renderer.LayerFrom  =  _layerFrom.Value;
			if (!_layerTo.IsNone)    renderer.LayerTo    =    _layerTo.Value;
		}
	}
}
