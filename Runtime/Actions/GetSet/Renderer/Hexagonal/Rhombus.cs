﻿using GridFramework.Renderers.Hexagonal;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessRhombusRenderer: AccessGridRenderer<Rhombus> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Rhombus))]
		[RequiredField]
		public FsmObject _renderer;

		[Tooltip("Lower edge of the renderer.")]
		public FsmInt _bottom;

		[Tooltip("Upper edge of the renderer.")]
		public FsmInt _top;

		[Tooltip("Left edge of the renderer.")]
		public FsmInt _left;

		[Tooltip("Right edge of the renderer.")]
		public FsmInt _right;

		[Tooltip("Lower layer range of the renderer.")]
		public FsmFloat _layerFrom;

		[Tooltip("Upper layer range of the renderer.")]
		public FsmFloat _layerTo;
	}

	public class GetRhombusRenderer: AccessRhombusRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Rhombus;
			this.GetValues(renderer);
			_bottom.Value = renderer.Bottom;
			_top.Value = renderer.Top;
			_left.Value = renderer.Left;
			_right.Value = renderer.Right;
			_layerFrom.Value = renderer.LayerFrom;
			_layerTo.Value = renderer.LayerTo;
		}
	}

	public class SetRhombusRenderer: AccessRhombusRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Rhombus;
			this.SetValues(renderer);
			if (!_bottom.IsNone) renderer.Bottom = _bottom.Value;
			if (!_top.IsNone) renderer.Top = _top.Value;
			if (!_left.IsNone) renderer.Left = _left.Value;
			if (!_right.IsNone) renderer.Right = _right.Value;
			if (!_layerFrom.IsNone) renderer.LayerFrom = _layerFrom.Value;
			if (!_layerTo.IsNone) renderer.LayerTo = _layerTo.Value;
		}
	}
}
