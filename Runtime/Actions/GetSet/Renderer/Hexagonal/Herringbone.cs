﻿using GridFramework.Renderers.Hexagonal;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessHerringboneRenderer: AccessGridRenderer<Herringbone> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Herringbone))]
		[RequiredField]
		public FsmObject _renderer;

		[Tooltip("Lower range of the renderer.")]
		public FsmVector3 _from;

		[Tooltip("Upper range of the renderer.")]
		public FsmVector3 _to;

		[Tooltip("How to shift every odd-numbered column")]
		[ObjectType(typeof(Herringbone.OddColumnShift))]
		public FsmEnum _shift;

	}
	public class GetHerringboneRenderer: AccessHerringboneRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Herringbone;
			this.GetValues(renderer);
			_from.Value = renderer.From;
			_to.Value = renderer.To;
			_shift.Value = renderer.Shift;
		}
	}

	public class SetHerringboneRenderer: AccessHerringboneRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Herringbone;
			this.SetValues(renderer);
			if (!_from.IsNone) renderer.From = _from.Value;
			if (!_to.IsNone) renderer.To = _to.Value;
			if (!_shift.IsNone) renderer.Shift = (Herringbone.OddColumnShift)_shift.Value;
		}
	}
}
