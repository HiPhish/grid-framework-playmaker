﻿using GridFramework.Renderers.Hexagonal;

namespace HutongGames.PlayMaker.Actions {
	public abstract class AccessRectangleRenderer: AccessGridRenderer<Rectangle> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Rectangle))]
		[RequiredField]
		public FsmObject _renderer;

		[Tooltip("Lower edge of the renderer.")]
		public FsmInt _bottom;

		[Tooltip("Upper edge of the renderer.")]
		public FsmInt _top;

		[Tooltip("Left edge of the renderer.")]
		public FsmInt _left;

		[Tooltip("Right edge of the renderer.")]
		public FsmInt _right;

		[Tooltip("Lower layer range of the renderer.")]
		public FsmFloat _layerFrom;

		[Tooltip("Upper layer range of the renderer.")]
		public FsmFloat _layerTo;

		[Tooltip("How to shift every odd-numbered column")]
		[ObjectType(typeof(Rectangle.OddColumnShift))]
		public FsmEnum _shift;
	}

	public class GetRectangleRenderer: AccessRectangleRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Rectangle;
			this.GetValues(renderer);
			_bottom.Value = renderer.Bottom;
			_top.Value = renderer.Top;
			_left.Value = renderer.Left;
			_right.Value = renderer.Right;
			_layerFrom.Value = renderer.LayerFrom;
			_layerTo.Value = renderer.LayerTo;
			_shift.Value = renderer.Shift;
		}
	}

	public class SetRectangleRenderer: AccessRectangleRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Rectangle;
			this.SetValues(renderer);
			if (!_bottom.IsNone) renderer.Bottom = _bottom.Value;
			if (!_top.IsNone) renderer.Top = _top.Value;
			if (!_left.IsNone) renderer.Left = _left.Value;
			if (!_right.IsNone) renderer.Right = _right.Value;
			if (!_layerFrom.IsNone) renderer.LayerFrom = _layerFrom.Value;
			if (!_layerTo.IsNone) renderer.LayerTo = _layerTo.Value;
			if (!_shift.IsNone) renderer.Shift = (Rectangle.OddColumnShift) _shift.Value;
		}
	}
}
