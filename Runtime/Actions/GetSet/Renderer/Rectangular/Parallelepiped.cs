﻿using GridFramework.Renderers.Rectangular;

namespace HutongGames.PlayMaker.Actions {
	public abstract class AccessParallelepiped: AccessGridRenderer<Parallelepiped> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Parallelepiped))]
		[RequiredField]
		public FsmObject _renderer;

		[Title("From")]
		[Tooltip("Set the lower range of the renderer.")]
		public FsmVector3 _from;

		[Title("To")]
		[Tooltip("Set the upper range of the renderer.")]
		public FsmVector3 _to;
	}

	[Tooltip("Gets the rendering range of a rectangular parallelepiped renderer")]
	public class GetParallelepiped: AccessParallelepiped {
		protected override void DoAction() {
			var renderer = _renderer.Value as Parallelepiped;
			this.GetValues(renderer);
			_from.Value = renderer.From;
			_to.Value = renderer.To;
		}
	}

	[Tooltip("Sets the rendering range of a rectangular parallelepiped renderer")]
	public class SetParallelepiped: AccessParallelepiped {
		protected override void DoAction() {
			var renderer = _renderer.Value as Parallelepiped;
			this.SetValues(renderer);
			if (!_from.IsNone)  renderer.From = _from.Value;
			if (!_to.IsNone)	  renderer.To = _to.Value;
		}
	}
}
