﻿using GridFramework.Renderers.Polar;

namespace HutongGames.PlayMaker.Actions.GridFramework {
	public abstract class AccessCylinderRenderer: AccessGridRenderer<Cylinder> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Cylinder))]
		[RequiredField]
		public FsmObject _renderer;

		[Tooltip("Lower radial range of the renderer.")]
		public FsmFloat _radialFrom;

		[Tooltip("Upper radial range of the renderer.")]
		public FsmFloat _radialTo;

		[Tooltip("Lower sector range of the renderer.")]
		public FsmFloat _sectorFrom;

		[Tooltip("Upper sector range of the renderer.")]
		public FsmFloat _sectorTo;

		[Tooltip("Lower layer range of the renderer.")]
		public FsmFloat _layerFrom;

		[Tooltip("Upper layer range of the renderer.")]
		public FsmFloat _layerTo;
	}

	public class GetCylinderRenderer: AccessCylinderRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Cylinder;
			this.GetValues(renderer);
			_radialFrom.Value = renderer.RadialFrom;
			_radialTo.Value = renderer.RadialTo;
			_sectorFrom.Value = renderer.SectorFrom;
			_sectorTo.Value = renderer.SectorTo;
			_layerFrom.Value = renderer.LayerFrom;
			_layerTo.Value = renderer.LayerTo;
		}
	}

	public class SetCylinderRenderer: AccessCylinderRenderer {
		protected override void DoAction() {
			var renderer = _renderer.Value as Cylinder;
			this.SetValues(renderer);
			if (!_radialFrom.IsNone) renderer.RadialFrom = _radialFrom.Value;
			if (!_radialTo.IsNone) renderer.RadialTo = _radialTo.Value;
			if (!_sectorFrom.IsNone) renderer.SectorFrom = _sectorFrom.Value;
			if (!_sectorTo.IsNone) renderer.SectorTo = _sectorTo.Value;
			if (!_layerFrom.IsNone) renderer.LayerFrom = _layerFrom.Value;
			if (!_layerTo.IsNone) renderer.LayerTo = _layerTo.Value;
		}
	}
}
