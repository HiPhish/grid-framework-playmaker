﻿using GridFramework.Renderers.Spherical;

namespace HutongGames.PlayMaker.Actions {
	public abstract class AccessSphere: AccessGridRenderer<Sphere> {
		[Tooltip("Renderer instance")]
		[ObjectType(typeof(Sphere))]
		[RequiredField]
		public FsmObject _renderer;

		[Title("Altitude from")]
		[Tooltip("Lower range of the renderer's altitude.")]
		public FsmFloat _altitudeFrom;

		[Title("Altitude to")]
		[Tooltip("Upper range of the renderer's altitude.")]
		public FsmFloat _altitudeTo;

		[Title("Longitude from")]
		[Tooltip("Lower range of the renderer's longitude.")]
		public FsmFloat _longitudeFrom;

		[Title("Longitude to")]
		[Tooltip("Upper range of the renderer's longitude.")]
		public FsmFloat _longitudeTo;

		[Title("Latitude from")]
		[Tooltip("Lower range of the renderer's latitude.")]
		public FsmFloat _latitudeFrom;

		[Title("Latitude to")]
		[Tooltip("Upper range of the renderer's latitude.")]
		public FsmFloat _latitudeTo;
	}

	public class GetSphere: AccessSphere {
		protected override void DoAction() {
			var renderer = _renderer.Value as Sphere;
			this.GetValues(renderer);
			_altitudeFrom.Value = renderer.AltFrom;
			_altitudeTo.Value = renderer.AltTo;
			_longitudeFrom.Value = renderer.LonFrom;
			_longitudeTo.Value = renderer.LonTo;
			_latitudeFrom.Value = renderer.LatFrom;
			_latitudeTo.Value = renderer.LatTo;
		}
	}

	public class SetSphere: AccessSphere {
		protected override void DoAction() {
			var renderer = _renderer.Value as Sphere;
			this.SetValues(renderer);
			if (!_altitudeFrom.IsNone) renderer.AltFrom = _altitudeFrom.Value;
			if (!_altitudeTo.IsNone) renderer.AltTo = _altitudeTo.Value;
			if (!_longitudeFrom.IsNone) renderer.LonFrom = _longitudeFrom.Value;
			if (!_longitudeTo.IsNone) renderer.LonTo = _longitudeTo.Value;
			if (!_latitudeFrom.IsNone) renderer.LatFrom = _latitudeFrom.Value;
			if (!_latitudeTo.IsNone) renderer.LatTo = _latitudeTo.Value;
		}
	}
}
