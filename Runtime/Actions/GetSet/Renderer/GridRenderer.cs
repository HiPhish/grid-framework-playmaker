﻿using GridFramework.Renderers;

namespace HutongGames.PlayMaker.Actions {
	public abstract class AccessGridRenderer<T>: FsmGFStateAction<T> where T: GridRenderer {
		[Title("X-axis color")]
		[Tooltip("Get the color of X-lines.")]
		public FsmColor _colorX;

		[Title("Y-axis color")]
		[Tooltip("Get the color of Y-lines.")]
		public FsmColor _colorY;

		[Title("Z-axis color")]
		[Tooltip("Get the color of Z-lines.")]
		public FsmColor _colorZ;

		[Tooltip("Width of the rendered lines.")]
		public FsmFloat _lineWidth;

		[Tooltip("Material of the rendered lines.")]
		public FsmMaterial _material;

		protected void GetValues(GridRenderer renderer) {
			_colorX.Value = renderer.ColorX;
			_colorY.Value = renderer.ColorY;
			_colorZ.Value = renderer.ColorZ;
			_lineWidth.Value = renderer.LineWidth;
			_material.Value = renderer.Material;
		}

		protected void SetValues(GridRenderer renderer) {
			if (!_colorX.IsNone) renderer.ColorX = _colorX.Value;
			if (!_colorY.IsNone) renderer.ColorY = _colorY.Value;
			if (!_colorZ.IsNone) renderer.ColorZ = _colorZ.Value;
			if (!_lineWidth.IsNone) renderer.LineWidth = _lineWidth.Value;
			if (!_material.IsNone) renderer.Material = _material.Value;
		}
	}
}
