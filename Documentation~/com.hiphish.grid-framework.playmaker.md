# Playmaker bindings for Grid Framework

Playmaker is a 3rd party Unity plugin which allows for visual scripting, and
Grid Framework offers support for it by exposing its API through Playmaker
actions.


## Package contents

- Playmaker actions: getters and setter for grid properties and renderer
	properties, coordinate conversion actions for each grid
- Samples to get started
- This documentation


## Installation instructions

Please refer to the official packages documentation on how to add a custom
package. I recommend adding this Git repository as a dependency to your project
and specifying the release tag you wish to use:

```json
{
  "dependencies": {
    "com.hiphish.grid-framework.playmaker": "https://gitlab.com/HiPhish/grid-framework-playmaker#v3.0.0"
  }
}
```

You will need Grid Framework and Playmaker installed for the code to compile
correctly.


## Requirements

- [Grid Framework](http://hiphish.github.io/grid-framework/)
- [Playmaker](https://hutonggames.com/)


## Getting started

There is a new actions category called "Grid Framework" which contains bindings
for the Grid Framework API. The actions can be grouped into three types:

- Getters for classes
- Setters for classes
- Coordinate conversion

Getters and setters allow you to access the values of any grid or renderer.
There is one getter and setter for each class. Coordinate conversion converts
coordinates from one coordinate system to another. There is one conversion
action per grid, and the action lets you specify the coordinate system to
convert from and to.

All actions have a "component" field, usually called "grid" or "renderer" for
the component to operate on. You will need to specify an FSM variable of type
`Object` and whose `ObjecType` value is set to the actual class of the
component. For example, if you want to set the field of a rectangular grid you
will need to create a variable, let's call it `grid` of type `Object` and with
an `ObjectType` of `GridFramework.Grids.RectGrid`.

Please see the included examples for practical examples.


## Samples

The following samples are included:


### Create grid

A demonstration of how to create a new grid object from scratch in the scene.
We create the object, then assign a grid component and a renderer component,
and finally we set the properties of the renderer.  The FSM is attached to the
camera, but it could be attached to any other object.


### Convert coordinates

Given a grid and a position in hex coordinates we convert it to a position in
world coordinates and instantiate an object there. This example also
demonstrates how we can get hold of an already existing grid.


### Resize grid

The radius of the polar grid is smoothly animated to grow and shrink. This
example uses Playmaker's built-in float animation action to modify a number
variable every frame. This variable is then assigned to the radius of the grid
on every frame, creating the pulsating grid. Note how all the fields we want to
remain unchanged have their value set to `None`.
