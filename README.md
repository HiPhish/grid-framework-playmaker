# Playmaker bindings for Grid Framework

This [Unity3D] package implements [Playmaker] bindings for [Grid Framework]. It
allows you to call the Grid Framework API using Playmaker's visual scripting
actions.


## Installation

This Git repository is a regular standalone Unity package. You can read more
about it in the [documentation].


## Usage

In order to use this package you should be familiar with both Grid Framework
and Playmaker.


## License

Released under the MIT license. Please see the [LICENSE] file for details.


[Unity3D]: https://unity.com/
[Playmaker]: https://hutonggames.com/
[Grid Framework]: http://hiphish.github.io/grid-framework/
[LICENSE]: LICENSE.md
[documentation]: Documentation~/com.hiphish.grid-framework.playmaker.md
